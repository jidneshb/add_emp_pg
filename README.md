
# add\_emp_pg container 

This is a form, where you add some info into your 'Employee' table.

## Getting Started

To start with you can visit my Docker hub repositories. 

* [add\_emp\_pg](https://hub.docker.com/repository/docker/jidneshb/add_emp_pg)
* [get\_emp\_pg](https://hub.docker.com/repository/docker/jidneshb/get_emp_pg)

Here I have some pre-build images to try with

|Tag Name|Description|
|:------:|:---------:|
|0.1|Is the first raw image, where you will not get anything working|
|0.2|This tag is configured with 127.0.0.1 (localhost), so if you run it properly you will have a working project|

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

_Also a local or cloud based **MySQL DB**_ 

### Usage

#### Flow of the pages 

```mermaid
graph LR
A(add_emp_pg) -- Write Data to --> B((emp_info:Employee))
B -- Pull Data from --> D(get_emp_pg)
```

#### DB Details 
**Table Name** : _Employee_
**DB Name** : _emp_info_


|Field|Type|
|:----:|:----:|
|emp_id| int(11)|
|first_name|varchar(40)|
|last_name|varchar(40)|
|primary_skills|varchar(40)||
|location|char(30)|

#### Environment Variables

* `DBHOST`   - Host Name of the DB
* `DBPORT`   - Port number 
* `DBUSER`   - User Name
* `DBPWD`    - Password
* `DATABASE` - DB Name

To make simple I created an environment.txt file to provide at run time.

```shell
--- environment.txt ---
DBHOST=x.x.x.x
DBPORT=3306
DBUSER=$$$$$
DBPWD=$$$$$
DATABASE=XXXX
```

#### To run the Containers

**MySQL DB is running on Port 3306**

* **Step 1 :** Run a MySQL Container 
```shell
 docker run --name mysql-instance -e MYSQL_ROOT_PASSWORD='admin#1234' -p 3306:3306 -d mysql:8.0.21
```

* **Step 2 :** Restore a Dump 
```shell
 docker exec -i mysql-instance sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < emp_info_dump.sql
```

* **Step Extra :** Alternate way to connect using HOST's mysql client 
```shell
 # 1 : docker inspect mysql-instance   ... Get the IP of your container e.g. 172.17.0.2
 # 2 : mysql -h 172.17.0.2 -uroot -padmin#1234
 # 3 : Change your environment.txt as per your MySQL connection settings 
```

**add\_emp\_pg is running on Port 80** 
```shell
 docker run --name addemp --env-file environment.txt -it -d -p 80:80 jidneshb/add_emp_pg:0.2
```

**get\_emp\_pg is running on Port 8080**
```shell
 docker run --name getemp --env-file environment.txt -it -d -p 8080:8080 jidneshb/get_emp_pg:0.2
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* I would like to thank Soumyadeep Dey, for his initial code to start with Containerization
